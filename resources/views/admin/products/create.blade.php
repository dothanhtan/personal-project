@extends('admin.layouts.app')
@section('title', 'Create Product')
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Create Product</h3>
        </div>
        <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="row my-3">
                    <div class="col-4">
                        <div class="input-group input-group-static">
                            <label class="ms-0" for="image">Image</label>
                            <input type="file" accept="image/*" class="form-control @error('image') is-invalid @enderror" id="image" name="image" value="{{old('image')}}">
                            @error('image')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                            <img src="" alt="" class="border mt-3" id="showImage" width="315">
                        </div>

                        <div class="mt-3">
                            <input type="hidden" id="inputSize" name='sizes'>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#AddSizeModal">
                                Add size
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="AddSizeModal" role="dialog" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title font-weight-normal">Add size</h5>
                                            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" id="AddSize">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn bg-gradient-primary btn-add-size">Add</button>
                                            <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="input-group input-group-static mb-3">
                            <label class="ms-0" for="name">Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="input-group input-group-static my-3">
                            <label class="ms-0" for="price">Price</label>
                            <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price">
                            @error('price')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="input-group input-group-static my-3">
                            <label class="ms-0" for="sale">Sale</label>
                            <input type="number" class="form-control @error('sale') is-invalid @enderror" id="sale" name="sale" value="{{old('sale')}}">
                            @error('sale')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="input-group input-group-static my-3">
                            <label class="ms-0" for="description">Description</label>
                            <textarea class="form-control @error('description') is-invalid @enderror" rows="2" id="description" name="description">{{old('description')}}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="input-group input-group-static mt-3">
                            <label class="ms-0" for="category_ids">Category</label>
                            <select name="category_ids[]" class="form-control @error('category_ids') is-invalid @enderror" id="category_ids" multiple>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            @error('category_ids')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>












            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js"></script>
    <script>let sizes = [{id: Date.now(), size: 'M', quantity: 1}];</script>
    <script src="{{ asset('admin/js/base/modal.js') }}"></script>
@endsection
