@extends('admin.layouts.app')
@section('title', 'Products')
@section('content')
    <div class="card">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible text-white fade show" role="alert">
                <span class="alert-icon align-middle">
                  <span class="material-icons text-md">thumb_up_off_alt</span>
                </span>
                <span class="alert-text">{{session('message')}}</span>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Product List</h3>
        </div>

        <div class="card-body">
            <form action="{{ route('products.index') }}" method="GET">
                <div class="row">
                    <div class="col-3">
                        <div class="input-group input-group-outline my-1">
                            @can('create-product')
                            <a href="{{route('products.create')}}" class="btn btn-primary">ADD PRODUCT</a>
                            @endcan
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="input-group input-group-outline my-1">
                            <input type="text" class="form-control" name="product_name" value="{{ $_GET['product_name'] ?? '' }}" placeholder="Name">
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="input-group input-group-outline my-1">
                            <input type="number" min="1" class="form-control" name="product_price" value="{{$_GET['product_price'] ?? 0 }}" placeholder="Price">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="input-group input-group-outline my-1">
                            <select name="category_name" class="form-control">
                                <option value="">Select Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->name }}" @selected(isset($_GET['category_name']) && ($_GET['category_name'] == $category->name))>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="input-group input-group-outline my-1">
                            <button type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-hover align-items-center border mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">ID</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7">Image</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Name</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Price</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Sale</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="align-middle">
                                {{$product->id}}
                            </td>
                            <td class="align-middle">
                                <img src="{{$product->image_path}}" alt="" width="100">
                            </td>
                            <td class="align-middle">
                                {{$product->name}}
                            </td>
                            <td class="align-middle">
                                ${{number_format($product->price)}}
                            </td>
                            <td class="align-middle">
                                {{$product->sale}}
                            </td>
                            <td class="align-middle">
                                @can('show-product')
                                <div>
                                    <a href="{{route('products.show', $product->id)}}" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                                </div>
                                @endcan

                                @can('update-product')
                                <div>
                                    <a href="{{route('products.edit', $product->id)}}" class="btn btn-sm btn-warning"><i class="fas fa-pencil"></i></a>
                                </div>
                                @endcan

                                @can('delete-product')
                                <form id="deleteForm{{$product->id}}" action="{{route('products.destroy', $product->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button data-id="deleteForm{{$product->id}}" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-close"></i></button>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="mt-5">
                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection
