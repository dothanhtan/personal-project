@extends('admin.layouts.app')
@section('title', 'Show Product', $product->name)
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Show Product</h3>
        </div>

        <div class="card-body">
            <div class="row my-3">
                <div class="col-4">
                    <label class="ms-0" for="image">Image</label>
                    <img src="{{$product->image_path}}" alt="Image" class="border" id="showImage" width="290">
                </div>

                <div class="col-8">
                    <div class="input-group input-group-static mb-3">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$product->name}}" readonly>
                    </div>

                    <div class="input-group input-group-static my-3">
                        <label class="ms-0" for="price">Price</label>
                        <input type="text" class="form-control" id="price" name="price" value="{{$product->price}}" readonly>
                    </div>

                    <div class="input-group input-group-static my-3">
                        <label class="ms-0" for="sale">Sale</label>
                        <input type="number" class="form-control" id="sale" name="sale" value="{{$product->sale}}" readonly>
                    </div>

                    <div class="input-group input-group-static my-3">
                        <label class="ms-0" for="description">Description</label>
                        <textarea class="form-control" rows="2" id="description" name="description" readonly>{{$product->description}}</textarea>
                    </div>
                </div>
            </div>

            @if ($product->details->count() > 0)
                <div class="row">
                    @foreach ($product->details as $detail)
                        <div class="col-6">
                            <div class="input-group input-group-static my-3">
                                <label class="ms-0" for="size">Size</label>
                                <input type="text" class="form-control" id="size" value="{{$detail->size}}" readonly>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="input-group input-group-static my-3">
                                <label class="ms-0" for="quantity">Quantity</label>
                                <input type="number" class="form-control" id="quantity" value="{{$detail->quantity}}" readonly>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="row">
                    <div class="input-group input-group-static my-3">
                        <label class="ms-0">This product has not entered the sizes yet</label>
                    </div>
                </div>
            @endif

            @if ($product->categories->count() > 0)
                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="category_ids">Category</label>
                    @foreach ($product->categories as $item)
                        <input type="text" class="form-control" id="category_ids{{$item->id}}" name="category_ids[]" value="{{$item->name}}" readonly />
                    @endforeach
                </div>
            @else
                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="category_ids">This product has not chosen the categories yet</label>
                </div>
            @endif
        </div>

        <div class="card-footer">
            <a href="{{route('products.index')}}" class="btn btn-secondary">Exit</a>
        </div>
    </div>
@endsection
