@extends('admin.layouts.app')
@section('title', 'Users')
@section('content')
    <div class="card">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible text-white fade show" role="alert">
                <span class="alert-icon align-middle">
                  <span class="material-icons text-md">thumb_up_off_alt</span>
                </span>
                <span class="alert-text">{{session('message')}}</span>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">User List</h3>
        </div>

        <div class="card-body">
            <form action="{{ route('users.index') }}" method="GET">
                <div class="row">
                    <div class="col-2">
                        <div class="input-group input-group-outline my-1">
                            @can('create-user')
                            <a href="{{route('users.create')}}" class="btn btn-primary">ADD USER</a>
                            @endcan
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="input-group input-group-outline my-1">
                            <input type="text" class="form-control" name="name" value="{{ $_GET['name'] ?? '' }}" placeholder="Name">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="input-group input-group-outline my-1">
                            <input type="email" class="form-control" name="email" value="{{$_GET['email'] ?? '' }}" placeholder="Email">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="input-group input-group-outline my-1">
                            <input type="tel" class="form-control" name="phone" value="{{$_GET['phone'] ?? '' }}" placeholder="Phone">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="input-group input-group-outline my-1">
                            <button type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-hover align-items-center border mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">ID</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7">Avatar</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Name</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Email</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Phone</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                {{$user->id}}
                            </td>
                            <td>
                                <img src="{{$user->image_path}}" alt="" width="100">
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                            <td>
                                {{$user->phone}}
                            </td>
                            <td>
                                @can('update-user')
                                <div>
                                    <a href="{{route('users.edit', $user->id)}}" class="btn btn-sm btn-warning"><i class="material-icons">edit</i></a>
                                </div>
                                @endcan

                                @can('delete-user')
                                <form id="deleteForm{{$user->id}}" action="{{route('users.destroy', $user->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button data-id="deleteForm{{$user->id}}" class="btn btn-sm btn-danger btn-delete"><i class="material-icons">close</i></button>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="mt-5">
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection
