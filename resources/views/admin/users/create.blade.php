@extends('admin.layouts.app')
@section('title', 'Create User')
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Create User</h3>
        </div>
        <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="row my-3">
                    <div class="col-4">
                        <div class="input-group input-group-static">
                            <label for="image">Avatar</label>
                            <input type="file" accept="image/*" class="form-control @error('image') is-invalid @enderror" id="image" name="image" value="{{old('image')}}">
                            @error('image')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                            <img src="" alt="" id="showImage" class="border mt-3" width="315">
                        </div>
                    </div>

                    <div class="col-8">
                        <div class="input-group input-group-static mb-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="input-group input-group-static my-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{old('email')}}">
                            @error('email')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="input-group input-group-static my-3">
                            <label for="password">Password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                            @error('password')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="input-group input-group-static my-3">
                            <label for="gender">Gender</label>
                            <select name="gender" class="form-control @error('gender') is-invalid @enderror" id="gender">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                            @error('gender')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="input-group input-group-static my-3">
                            <label for="phone">Phone</label>
                            <input type="tel" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{old('phone')}}">
                            @error('phone')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="address">Address</label>
                    <textarea class="form-control @error('address') is-invalid @enderror" rows="2" id="address" name="address">{{old('address')}}</textarea>
                    @error('address')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="ms-0">Role</label>
                    @foreach($roles as $groupName => $role)
                    <div class="row">
                        <h6>{{$groupName}}</h6>
                        @foreach($role as $item)
                        <div class="col-2">
                            <div class="form-check" style="padding-left: 0">
                                <input class="form-check-input" type="checkbox" value="{{$item->id}}" name="role_ids[]" id="customCheck">
                                <label class="custom-control-label" for="customCheck">{{$item->display_name}}</label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
