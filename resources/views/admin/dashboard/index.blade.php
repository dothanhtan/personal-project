@extends('admin.layouts.app')
@section('content')
<div class="row" style="margin-bottom: calc(100vh - 435px);">
    <div class="col-xl-4 col-sm-6 mb-xl-4 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">content_paste</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Today's Roles</p>
                    <h4 class="mb-0">{{$roleCount}}</h4>
                </div>
            </div>
            <div class="card-footer p-3">
                <p class="mb-0"></p>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-xl-4 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">person</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Today's Users</p>
                    <h4 class="mb-0">{{$userCount}}</h4>
                </div>
            </div>
            <div class="card-footer p-3">
                <p class="mb-0"></p>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-xl-4 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div class="icon icon-lg icon-shape bg-gradient-secondary shadow-secondary text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">category</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Today's Categories</p>
                    <h4 class="mb-0">{{$categoryCount}}</h4>
                </div>
            </div>
            <div class="card-footer p-3">
                <p class="mb-0"></p>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-xl-4 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div class="icon icon-lg icon-shape bg-gradient-warning shadow-warning text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">qr_code</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Today's Products</p>
                    <h4 class="mb-0">{{$productCount}}</h4>
                </div>
            </div>
            <div class="card-footer p-3">
                <p class="mb-0"></p>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-xl-4 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div class="icon icon-lg icon-shape bg-gradient-danger shadow-danger text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">percent</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Today's Coupons</p>
                    <h4 class="mb-0">{{$couponCount}}</h4>
                </div>
            </div>
            <div class="card-footer p-3">
                <p class="mb-0"></p>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-xl-4 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">list_alt</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Today's Orders</p>
                    <h4 class="mb-0">{{$orderCount}}</h4>
                </div>
            </div>
            <div class="card-footer p-3">
                <p class="mb-0"></p>
            </div>
        </div>
    </div>
</div>
@endsection
