@extends('admin.layouts.app')
@section('title', 'Edit Category', $category->name)
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Edit Category</h3>
        </div>
        <form action="{{ route('categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="name">Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name') ?? $category->name}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>

                @if($category->childrens->count() < 1)
                    <div class="input-group input-group-static my-3">
                        <label class="ms-0" for="parent_id">Parent Category</label>
                        <select name="parent_id" class="form-control @error('parent_id') is-invalid @enderror" id="parent_id">
                            <option value="">Select parent category</option>
                            @foreach($categories as $item)
                                <option value="{{$item->id}}" @selected((old('parent_id') ?? $category->parent_id) == $item->id)>{{$item->name}}</option>
                            @endforeach
                        </select>
                        @error('parent_id')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{route('categories.index')}}" class="btn btn-secondary">Cancel</a>
            </div>
        </form>
    </div>
@endsection
