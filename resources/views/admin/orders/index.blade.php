@extends('admin.layouts.app')
@section('title', 'Orders')
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Order List</h3>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover align-items-center border mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">ID</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Name</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Email</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Address</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Note</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Payment</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Ship</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Total</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>
                                {{$order->id}}
                            </td>
                            <td>
                                {{$order->customer_name}}
                            </td>
                            <td>
                                {{$order->customer_email}}
                            </td>
                            <td>
                                {{$order->customer_address}}
                            </td>
                            <td>
                                {{$order->note}}
                            </td>
                            <td>
                                {{$order->payment}}
                            </td>
                            <td>
                                ${{$order->ship}}
                            </td>
                            <td>
                                ${{number_format($order->total)}}
                            </td>
                            <td>
                                <div class="input-group input-group-outline">
                                    <select name="status" class="form-control select-status" data-action="{{route('orders.update', $order->id)}}">
                                        @foreach(config('order.status') as $status)
                                        <option value="{{$status}}" @selected($status == $order->status)>{{$status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="mt-5">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('basic/basic.js')}}"></script>
    <script src="{{asset('admin/app/order/order.js')}}"></script>
@endsection
