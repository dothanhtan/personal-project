@extends('admin.layouts.app')
@section('title', 'Create Role')
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Create Role</h3>
        </div>
        <form action="{{ route('roles.store') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="name">Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>

                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="display_name">Display Name</label>
                    <input type="text" class="form-control @error('display_name') is-invalid @enderror" id="display_name" name="display_name" value="{{old('display_name')}}">
                    @error('display_name')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label class="ms-0" for="group">Group</label>
                    <select name="group" class="form-control @error('group') is-invalid @enderror" id="group">
                        <option value="system">System</option>
                        <option value="user">User</option>
                    </select>
                    @error('group')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="ms-0">Permission</label>
                    <div class="row">
                        @foreach($permissions as $groupName => $permission)
                            <div class="col-2">
                                <h6>{{$groupName}}</h6>
                                @foreach($permission as $item)
                                    <div class="form-check" style="padding-left: 0">
                                        <input class="form-check-input" type="checkbox" value="{{$item->id}}" name="permission_ids[]">
                                        <label class="custom-control-label" for="customCheck">{{$item->display_name}}</label>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
