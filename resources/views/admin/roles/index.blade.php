@extends('admin.layouts.app')
@section('title', 'Roles')
@section('content')
    <div class="card">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible text-white fade show" role="alert">
                <span class="alert-icon align-middle">
                  <span class="material-icons text-md">thumb_up_off_alt</span>
                </span>
                <span class="alert-text">{{session('message')}}</span>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Role List</h3>
        </div>

        <div class="card-body">
            <div>
                @can('create-role')
                <a href="{{route('roles.create')}}" class="btn btn-sm btn-primary">ADD ROLE</a>
                @endcan
            </div>

            <div class="table-responsive">
                <table class="table table-hover align-items-center border mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">ID</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Name</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Display Name</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Group</th>
                            <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td>
                                {{$role->id}}
                            </td>
                            <td>
                                {{$role->name}}
                            </td>
                            <td>
                                {{$role->display_name}}
                            </td>
                            <td>
                                {{$role->group}}
                            </td>
                            <td class="d-flex">
                                @can('update-role')
                                <div style="margin-right: 1rem">
                                    <a href="{{route('roles.edit', $role->id)}}" class="btn btn-sm btn-warning"><i class="fas fa-pencil"></i></a>
                                </div>
                                @endcan

                                @can('delete-role')
                                <form id="deleteForm{{$role->id}}" action="{{route('roles.destroy', $role->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button data-id="deleteForm{{$role->id}}" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-close"></i></button>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="mt-5">
                {{ $roles->links() }}
            </div>
        </div>
    </div>
@endsection
