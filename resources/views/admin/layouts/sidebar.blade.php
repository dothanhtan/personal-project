<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href=" https://demos.creative-tim.com/material-dashboard/pages/dashboard " target="_blank">
            <i class="fas fa-user text-white"></i>
            <span class="ms-1 font-weight-bold text-white">{{ Auth::user()->name }}</span>
        </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse w-auto" id="sidenav-collapse-main" style="height: calc(100vh - 250px)">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-white {{request()->routeIs('dashboard') ? 'active bg-gradient-primary' : ''}}" href="{{route('dashboard')}}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">dashboard</i>
                    </div>
                    <span class="nav-link-text ms-1">Dashboard</span>
                </a>
            </li>

            @hasrole('super-admin')
            <li class="nav-item">
                <a class="nav-link text-white {{request()->routeIs('roles.*') ? 'active bg-gradient-primary' : ''}}" href="{{route('roles.index')}}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">content_paste</i>
                    </div>
                    <span class="nav-link-text ms-1">Role</span>
                </a>
            </li>
            @endhasrole

            @can('show-user')
                <li class="nav-item">
                    <a class="nav-link text-white {{request()->routeIs('users.*') ? 'active bg-gradient-primary' : ''}}" href="{{route('users.index')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">person</i>
                        </div>
                        <span class="nav-link-text ms-1">User</span>
                    </a>
                </li>
            @endcan

            @can('show-product')
                <li class="nav-item">
                    <a class="nav-link text-white {{request()->routeIs('products.*') ? 'active bg-gradient-primary' : ''}}" href="{{route('products.index')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">qr_code</i>
                        </div>
                        <span class="nav-link-text ms-1">Product</span>
                    </a>
                </li>
            @endcan

            @can('show-category')
                <li class="nav-item">
                    <a class="nav-link text-white {{request()->routeIs('categories.*') ? 'active bg-gradient-primary' : ''}}" href="{{route('categories.index')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">category</i>
                        </div>
                        <span class="nav-link-text ms-1">Category</span>
                    </a>
                </li>
            @endcan

            @can('show-coupon')
                <li class="nav-item">
                    <a class="nav-link text-white {{request()->routeIs('coupons.*') ? 'active bg-gradient-primary' : ''}}" href="{{route('coupons.index')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">percent</i>
                        </div>
                        <span class="nav-link-text ms-1">Coupon</span>
                    </a>
                </li>
            @endcan

            @can('list-order')
                <li class="nav-item">
                    <a class="nav-link text-white {{request()->routeIs('orders.*') ? 'active bg-gradient-primary' : ''}}" href="{{route('orders.index')}}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">list_alt</i>
                        </div>
                        <span class="nav-link-text ms-1">Order</span>
                    </a>
                </li>
            @endcan
        </ul>
    </div>
</aside>
