@extends('admin.layouts.app')
@section('title', 'Create Coupon')
@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Create Coupon</h3>
        </div>
        <form action="{{ route('coupons.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="name">Name</label>
                    <input type="text" class="form-control text-uppercase @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>

                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="value">Value</label>
                    <input type="number" class="form-control @error('value') is-invalid @enderror" id="value" name="value" value="{{old('value')}}">
                    @error('value')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>

                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="type">Type</label>
                    <select name="type" class="form-control @error('type') is-invalid @enderror" id="type">
                        <option>Select type</option>
                        <option value="money">Money</option>
                    </select>
                    @error('type')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>

                <div class="input-group input-group-static my-3">
                    <label class="ms-0" for="expery_date">Expery Date</label>
                    <input type="date" class="form-control @error('expery_date') is-invalid @enderror" id="expery_date" name="expery_date" value="{{old('expery_date')}}">
                    @error('expery_date')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
