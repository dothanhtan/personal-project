@extends('admin.layouts.app')
@section('title', 'Coupons')
@section('content')
    <div class="card">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible text-white fade show" role="alert">
                <span class="alert-icon align-middle">
                  <span class="material-icons text-md">thumb_up_off_alt</span>
                </span>
                <span class="alert-text">{{session('message')}}</span>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Coupon List</h3>
        </div>

        <div class="card-body">
            <form action="{{ route('coupons.index') }}" method="GET">
                <div class="row">
                    <div class="col-8">
                        <div class="input-group input-group-outline my-1">
                            <a href="{{route('coupons.create')}}" class="btn btn-primary">ADD COUPON</a>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="input-group input-group-outline my-1">
                            <input type="text" class="form-control" name="coupon_name" value="{{ $_GET['coupon_name'] ?? '' }}" placeholder="Name">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="input-group input-group-outline my-1">
                            <button type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-hover align-items-center border mb-0">
                    <thead>
                    <tr>
                        <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">ID</th>
                        <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Name</th>
                        <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Type</th>
                        <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Value</th>
                        <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Expery Date</th>
                        <th class="text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($coupons as $coupon)
                        <tr>
                            <td>
                                {{$coupon->id}}
                            </td>
                            <td>
                                {{$coupon->name}}
                            </td>
                            <td>
                                {{$coupon->type}}
                            </td>
                            <td>
                                {{$coupon->value}}
                            </td>
                            <td>
                                {{$coupon->expery_date}}
                            </td>
                            <td class="d-flex">
                                <div style="margin-right: 1rem">
                                    <a href="{{route('coupons.edit', $coupon->id)}}" class="btn btn-sm btn-warning"><i class="fas fa-pencil"></i></a>
                                </div>
                                <form id="deleteForm{{$coupon->id}}" action="{{route('coupons.destroy', $coupon->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button data-id="deleteForm{{$coupon->id}}" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-close"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="mt-5">
                {{ $coupons->links() }}
            </div>
        </div>
    </div>
@endsection
