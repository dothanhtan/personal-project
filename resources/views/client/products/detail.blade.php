@extends('client.layouts.app')
@section('title', 'Shop Detail')
@section('content')
<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="{{route('client.home')}}">Home</a>
                <a class="breadcrumb-item text-dark" href="{{route('client.about')}}">About</a>
                <a class="breadcrumb-item text-dark" href="{{route('client.shop')}}">Shop</a>
                <span class="breadcrumb-item active">Shop Detail</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- Shop Detail Start -->
<div class="container-fluid pb-5">
    <div class="row px-xl-5">
        <div class="col-12">
            @if(session('message'))
                <div class="alert alert-info alert-dismissible text-dark font-weight-bold fade show" role="alert">
                    <span class="alert-icon align-middle">
                        <span class="material-icons text-md">thumb_up_off_alt</span>
                    </span>
                    <span class="alert-text">{{session('message')}}</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>

    <form action="{{route('client.carts.add')}}" method="POST" class="row px-xl-5">
        @csrf
        <input type="hidden" name="product_id" value="{{$product->id}}">
        <div class="col-lg-5 mb-30">
            <div id="product-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner bg-light">
                    <div class="carousel-item active">
                        <img class="w-100 h-100" src="{{$product->image_path}}" alt="Image">
                    </div>
                    <div class="carousel-item">
                        <img class="w-100 h-100" src="{{$product->image_path}}" alt="Image">
                    </div>
                    <div class="carousel-item">
                        <img class="w-100 h-100" src="{{$product->image_path}}" alt="Image">
                    </div>
                    <div class="carousel-item">
                        <img class="w-100 h-100" src="{{$product->image_path}}" alt="Image">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#product-carousel" data-slide="prev">
                    <i class="fa fa-2x fa-angle-left text-dark"></i>
                </a>
                <a class="carousel-control-next" href="#product-carousel" data-slide="next">
                    <i class="fa fa-2x fa-angle-right text-dark"></i>
                </a>
            </div>
        </div>

        <div class="col-lg-7 h-auto mb-30">
            <div class="h-100 bg-light p-30">
                <h3>{{$product->name}}</h3>
                <div class="d-flex mb-3">
                    <div class="text-primary mr-2">
                        @php $rating = 3.5; @endphp
                        @foreach(range(1,5) as $i)
                            @if($rating > 0)
                                @if($rating > 0.5)
                                    <small class="fas fa-star"></small>
                                @else
                                    <small class="fas fa-star-half-alt"></small>
                                @endif
                            @else
                                <small class="far fa-star"></small>
                            @endif
                            @php $rating-- @endphp
                        @endforeach
                    </div>
                    <small class="pt-1">(99 Reviews)</small>
                </div>
                <h3 class="font-weight-semi-bold mb-4">${{number_format($product->price - $product->price * 0.2)}}</h3>
                <p class="text-justify mb-4">{{$product->description}}</p>

                @if($product->details->count() > 0)
                    <div class="d-flex mb-3">
                        <strong class="text-dark mr-3">Sizes:</strong>
                        <form>
                            @foreach($product->details as $size)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="size{{$size->id}}" name="product_size" value="{{$size->size}}">
                                    <label class="custom-control-label" for="size{{$size->id}}">{{$size->size}}</label>
                                </div>
                            @endforeach
                        </form>
                    </div>
                @else
                    <h4 class="text-uppercase font-weight-bold text-danger">out of stock</h4>
                @endif

                <div class="d-flex align-items-center mb-4 pt-2">
                    <div class="input-group quantity mr-3" style="width: 130px;">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-primary btn-minus" >
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <input type="text" class="form-control bg-secondary text-center" value="1" />
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-primary btn-plus">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary px-3">
                        <i class="fa fa-shopping-cart mr-1"></i> Add To Cart
                    </button>
                </div>

                <div class="d-flex pt-2">
                    <strong class="text-dark mr-2">Share on:</strong>
                    <div class="d-inline-flex">
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-pinterest"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="row px-xl-5">
        <div class="col">
            <div class="bg-light p-30">
                <div class="nav nav-tabs mb-4">
                    <a class="nav-item nav-link text-dark active" data-toggle="tab" href="#tab-pane-1">Description</a>
                    <a class="nav-item nav-link text-dark" data-toggle="tab" href="#tab-pane-2">Information</a>
                    <a class="nav-item nav-link text-dark" data-toggle="tab" href="#tab-pane-3">Reviews (1)</a>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <h4 class="mb-3">Product Description</h4>
                        <p class="text-justify">Eos no lorem usermod diam diam, eos elite et evergreen diam sea. Consecrate vero aliquot invidious duo dolores et duo sit. Vero diam ea vero et color rebut, dolor rebut usermod consecrate invidious sed sed et, lorem duo et eos elite, disciplining asd ipsum rebut diam. Dolores diam stet rebut sed temper asd usermod. animate asd ipsum accused disciplining, eos dolores sit no ut diam consecrate duo justo est, sit sanctums diam temper aliquot usermod noname rebut dolor accused, ipsum asd eos consecrate at sit rebut, diam asd invidious temper lorem, ipsum lorem elite sanctums usermod animate dolor ea invidious.</p>
                        <p class="text-justify">Dolores magna est usermod sanctums dolor, amet diam et usermod et ipsum. Amet color temper consecrate sed lorem dolor sit lorem temper. Gutenberg amet amet labor disciplining client client diam client. Sea amet et sed ipsum lorem elite et, amet et labor voluptuous sit rebut. Ea erat sed et diam animate sed justo. Magna animate justo et amet magna et.</p>
                    </div>
                    <div class="tab-pane fade" id="tab-pane-2">
                        <h4 class="mb-3">Additional Information</h4>
                        <p class="text-justify">Eos no lorem usermod diam diam, eos elite et evergreen diam sea. Consecrate vero aliquot invidious duo dolores et duo sit. Vero diam ea vero et color rebut, dolor rebut usermod consecrate invidious sed sed et, lorem duo et eos elite, disciplining asd ipsum rebut diam. Dolores diam stet rebut sed temper asd usermod. animate asd ipsum accused disciplining, eos dolores sit no ut diam consecrate duo justo est, sit sanctums diam temper aliquot usermod noname rebut dolor accused, ipsum asd eos consecrate at sit rebut, diam asd invidious temper lorem, ipsum lorem elite sanctums usermod animate dolor ea invidious.</p>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item px-0">
                                        Sit erat duo lorem duo ea consecrate, et usermod animate.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Amet asd evergreen sit sanctums et lorem eos disciplining at.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Duo amet accused usermod noname stet et et stet usermod.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Animate ea client labor amet ipsum erat justo voluptuous.
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item px-0">
                                        Sit erat duo lorem duo ea consecrate, et usermod animate.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Amet asd evergreen sit sanctums et lorem eos disciplining at.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Duo amet accused usermod noname stet et et stet usermod.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Animate ea client labor amet ipsum erat justo voluptuous.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-pane-3">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="mb-4">1 review for "{{$product->name}}"</h4>
                                <div class="media mb-4">
                                    <img src="{{asset('client/img/user.jpg')}}" alt="Image" class="img-fluid mr-3 mt-1" style="width: 45px;">
                                    <div class="media-body">
                                        <h6>John Doe<small> - <i>01 Jan 2020</i></small></h6>
                                        <div class="text-primary mb-2">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star-half-alt"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <p class="text-justify">Diam amet duo labor stet elite ea client ipsum, temper labor accused ipsum et no at. Wasd diam temper rebut magna dolores sed usermod ipsum.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <h4 class="mb-4">Leave a review</h4>
                                <small>Your email address will not be published. Required fields are marked *</small>
                                <div class="d-flex my-3">
                                    <p class="mb-0 mr-2">Your Rating * :</p>
                                    <div class="text-primary">
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                </div>
                                <form>
                                    <div class="form-group">
                                        <label for="message">Your Review *</label>
                                        <textarea id="message" cols="30" rows="3" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Your Name *</label>
                                        <input type="text" class="form-control" id="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Your Email *</label>
                                        <input type="email" class="form-control" id="email">
                                    </div>
                                    <div class="form-group mb-0">
                                        <input type="submit" value="Leave Your Review" class="btn btn-primary px-3">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Shop Detail End -->

<!-- Products Start -->
<div class="container-fluid py-5">
    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4">
        <span class="bg-secondary pr-3">You May Also Like</span>
    </h2>
    <div class="row px-xl-5">
        <div class="col">
            <div class="owl-carousel related-carousel">
                @foreach($products as $item)
                <div class="product-item bg-light">
                    <div class="product-img position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="{{$item->image_path}}" alt="">
                        <div class="product-action">
                            <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                            <a class="btn btn-outline-dark btn-square" href="{{route('client.products.show', $item->id)}}"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                        </div>
                    </div>
                    <div class="text-center py-4">
                        <a class="h6 text-decoration-none text-truncate" href="{{route('client.products.show', $item->id)}}">{{$product->name}}</a>
                        <div class="d-flex align-items-center justify-content-center mt-2">
                            <h5>${{number_format($item->price - $item->price * 0.2)}}</h5><h6 class="text-muted ml-2"><del>${{number_format($item->price)}}</del></h6>
                        </div>
                        <div class="d-flex align-items-center justify-content-center mb-1">
                            @php $rating = 3.5; @endphp
                            @foreach(range(1,5) as $i)
                                @if($rating > 0)
                                    @if($rating > 0.5)
                                        <small class="fas fa-star text-primary mr-1"></small>
                                    @else
                                        <small class="fas fa-star-half-alt text-primary mr-1"></small>
                                    @endif
                                @else
                                    <small class="far fa-star text-primary mr-1"></small>
                                @endif
                                @php $rating-- @endphp
                            @endforeach
                            <small>(99)</small>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- Products End -->
@endsection
