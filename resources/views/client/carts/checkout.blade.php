@extends('client.layouts.app')
@section('title', 'Checkout')
@section('content')
<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="{{route('client.home')}}">Home</a>
                <a class="breadcrumb-item text-dark" href="{{route('client.about')}}">About</a>
                <a class="breadcrumb-item text-dark" href="{{route('client.shop')}}">Shop</a>
                <span class="breadcrumb-item active">Checkout</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- Checkout Start -->
<div class="container-fluid">
    <form action="{{route('client.checkout.process')}}" method="post" class="row px-xl-5">
        @csrf
        <div class="col-lg-4">
            <h5 class="section-title position-relative text-uppercase mb-3">
                <span class="bg-secondary pr-3">Billing Address</span>
            </h5>
            <div class="bg-light p-30 mb-5">
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="customer_name">Name</label>
                        <input id="customer_name" name="customer_name" class="form-control @error('customer_name') is-invalid @enderror" type="text" value="{{old('customer_name')}}" placeholder="John">
                        @error('customer_name')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>

                    <div class="col-12 form-group">
                        <label for="customer_email">Email</label>
                        <input id="customer_email" name="customer_email" class="form-control @error('customer_email') is-invalid @enderror" type="email" value="{{old('customer_email')}}" placeholder="example@email.com">
                        @error('customer_email')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>

                    <div class="col-12 form-group">
                        <label for="customer_phone">Phone</label>
                        <input id="customer_phone" name="customer_phone" class="form-control @error('customer_phone') is-invalid @enderror" type="tel" value="{{old('customer_phone')}}" placeholder="0123 456 789">
                        @error('customer_phone')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>

                    <div class="col-12 form-group">
                        <label for="customer_address">Address</label>
                        <input id="customer_address" name="customer_address" class="form-control @error('customer_address') is-invalid @enderror" type="text" value="{{old('customer_address')}}" placeholder="123 Golden Dragon Street">
                        @error('customer_address')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>

                    <div class="col-12 form-group">
                        <label for="note">Note</label>
                        <input id="note" name="note" class="form-control @error('note') is-invalid @enderror" type="text" value="{{old('note')}}" placeholder="New York">
                        @error('note')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <h5 class="section-title position-relative text-uppercase mb-3">
                <span class="bg-secondary pr-3">Order Total</span>
            </h5>
            <div class="bg-light p-30 mb-5">
                <div class="border-bottom">
                    <h6 class="mb-3">Products</h6>
                    @foreach($cart->products as $item)
                        <div class="d-flex justify-content-between">
                            <div class="d-flex">
                                <p>{{ $item->product_quantity }}</p>
                                <p class="mx-2">x</p>
                                <p style="white-space: nowrap;max-width: 180px;overflow-x: hidden;text-overflow: ellipsis;">{{ $item->product->name }}</p>
                            </div>
                            <div class="d-flex">
                                <p style="{{ $item->product->sale ? 'text-decoration: line-through' : '' }}">
                                    ${{number_format($item->product_quantity * $item->product->price)}}
                                </p>
                                @if ($item->product->sale)
                                    @php($salePrice = $item->product_quantity * $item->product->sale_price)
                                    <p class="ml-3">${{number_format($salePrice)}}</p>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="border-bottom pt-3 pb-2">
                    <div class="d-flex justify-content-between mb-3">
                        <h6 class="font-weight-medium">Subtotal</h6>
                        <h6 class="font-weight-medium subtotal" data-price="{{$cart->total_price}}">${{number_format($cart->total_price)}}</h6>
                    </div>

                    @if(session('coupon_value'))
                        <div class="d-flex justify-content-between mb-3">
                            <h6 class="font-weight-medium">Coupon</h6>
                            <h6 class="font-weight-medium coupon" data-price="{{session('coupon_value')}}">${{session('coupon_value')}}</h6>
                        </div>
                    @endif

                    <div class="d-flex justify-content-between">
                        <h6 class="font-weight-medium">Shipping</h6>
                        <h6 class="font-weight-medium shipping" data-price="10">$10</h6>
                        <input type="hidden" name="ship" value="10">
                    </div>
                </div>
                <div class="pt-2">
                    <div class="d-flex justify-content-between mt-2">
                        <h5 class="font-weight-bold">Total</h5>
                        <h5 class="font-weight-bold total"></h5>
                        <input type="hidden" id="total" name="total" value="">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <h5 class="section-title position-relative text-uppercase mb-3">
                <span class="bg-secondary pr-3">Payment</span>
            </h5>
            <div class="bg-light p-30">
                <div class="form-group">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="payment" id="cash" value="cash" checked>
                        <label class="custom-control-label" for="cash">Cash</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="payment" id="transfer" value="transfer">
                        <label class="custom-control-label" for="transfer">Transfer</label>
                    </div>
                </div>
                <div class="form-group mb-4">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="payment" id="paypal" value="paypal">
                        <label class="custom-control-label" for="paypal">Paypal</label>
                    </div>
                </div>
                <button class="btn btn-block btn-primary font-weight-bold py-3">Place Order</button>
            </div>
        </div>
    </form>
</div>
<!-- Checkout End -->
@endsection
@section('script')
    <script src="{{asset('basic/basic.js')}}"></script>
    <script src="{{asset('client/app/cart/cart.js')}}"></script>
@endsection
