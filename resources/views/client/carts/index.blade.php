@extends('client.layouts.app')
@section('title', 'Shopping Cart')
@section('content')
<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="{{route('client.home')}}">Home</a>
                <a class="breadcrumb-item text-dark" href="{{route('client.about')}}">About</a>
                <a class="breadcrumb-item text-dark" href="{{route('client.shop')}}">Shop</a>
                <span class="breadcrumb-item active">Shopping Cart</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- Cart Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-lg-12 mb-3">
            @if(session('message'))
                <div class="alert alert-info alert-dismissible text-dark font-weight-bold fade show" role="alert">
                    <span class="alert-icon align-middle">
                        <span class="material-icons text-md">thumb_up_off_alt</span>
                    </span>
                    <span class="alert-text">{{session('message')}}</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>
    <div class="row px-xl-5">
        <div class="col-lg-8 table-responsive mb-5">
            <table class="table table-light table-borderless table-hover text-center mb-0">
                <thead class="thead-dark">
                    <tr>
                        <th>Image</th>
                        <th>Products</th>
                        <th>Price</th>
                        <th>Size</th>
                        <th>Sale</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody class="align-middle">
                @foreach($cart->products as $item)
                    <tr id="row-{{$item->id}}">
                        <td class="align-middle">
                            <img src="{{$item->product->image_path}}" alt="" style="width: 50px;">
                        </td>
                        <td class="align-middle">
                            <span style="overflow: hidden;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">{{$item->product->name}}</span>
                        </td>
                        <td class="align-middle">
                            <span style="{{ $item->product->sale ? 'text-decoration:line-through' : '' }}">
                                ${{number_format($item->product->price)}}
                            </span>

                            @if ($item->product->sale)
                                <span class="d-block">${{number_format($item->product->sale_price)}}</span>
                            @endif
                        </td>
                        <td class="align-middle">
                            {{$item->product_size}}
                        </td>
                        <td class="align-middle">
                            {{$item->product->sale}}
                        </td>
                        <td class="align-middle">
                            <div class="input-group quantity mx-auto" style="width: 100px;">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-primary btn-minus btn-update-quantity" data-action="{{route('client.carts.update', $item->id)}}" data-id="{{$item->id}}">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <input id="productQuantityInput-{{$item->id}}" class="form-control form-control-sm bg-secondary text-center" min="1" value="{{$item->product_quantity}}">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-primary btn-plus btn-update-quantity" data-action="{{route('client.carts.update', $item->id)}}" data-id="{{$item->id}}">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td class="align-middle">
                            <span id="cartProductPrice-{{ $item->id }}">
                                @php($priceSale = $item->product->sale_price * $item->product_quantity)
                                @php($price = $item->product->price * $item->product_quantity)
                                ${{ $item->product->sale ? number_format($priceSale) : number_format($price)}}
                            </span>
                        </td>
                        <td class="align-middle">
                            <button class="btn btn-sm btn-danger btn-remove-product" data-action="{{route('client.carts.remove', $item->id)}}"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-lg-4">
            <form class="mb-30" action="{{route('client.carts.coupon')}}" method="post">
                @csrf
                <div class="input-group">
                    <input id="coupon_code" name="coupon_code" value="{{Session::get('coupon_code')}}" class="form-control border-0 p-4" placeholder="Coupon Code">
                    <div class="input-group-append"><button class="btn btn-primary">Apply Coupon</button></div>
                </div>
            </form>

            <h5 class="section-title position-relative text-uppercase mb-3">
                <span class="bg-secondary pr-3">Cart Summary</span>
            </h5>
            <div class="bg-light p-30 mb-5">
                <div class="border-bottom pb-2">
                    <div class="d-flex justify-content-between mb-3">
                        <h6 class="font-weight-medium">Subtotal</h6>
                        <h6 id="subtotal" class="font-weight-medium subtotal" data-price="{{$cart->total_price}}">${{number_format($cart->total_price)}}</h6>
                    </div>

                    @if(session('coupon_value'))
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">Coupon</h6>
                            <h6 class="font-weight-medium coupon" data-price="{{session('coupon_value')}}">${{session('coupon_value')}}</h6>
                        </div>
                    @endif
                </div>
                <div class="pt-2">
                    <div class="d-flex justify-content-between mt-2">
                        <h5 class="font-weight-bold">Total</h5>
                        <h5 class="font-weight-bold total"></h5>
                    </div>
                    <a href="{{route('client.checkout.index')}}" class="btn btn-block btn-primary font-weight-bold my-3 py-3">
                        Proceed To Checkout
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Cart End -->
@endsection
@section('script')
    <script src="{{asset('basic/basic.js')}}"></script>
    <script src="{{asset('client/app/cart/cart.js')}}"></script>
@endsection
