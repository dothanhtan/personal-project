@extends('client.layouts.app')
@section('title', 'Order')
@section('content')
<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="{{route('client.home')}}">Home</a>
                <a class="breadcrumb-item text-dark" href="{{route('client.about')}}">About</a>
                <a class="breadcrumb-item text-dark" href="{{route('client.shop')}}">Shop</a>
                <span class="breadcrumb-item active">Order</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- Cart Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-lg-12 mb-3">
            @if(session('message'))
                <div class="alert alert-info alert-dismissible text-dark font-weight-bold fade show" role="alert">
                    <span class="alert-icon align-middle">
                        <span class="material-icons text-md">thumb_up_off_alt</span>
                    </span>
                    <span class="alert-text">{{session('message')}}</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>

    <div class="row px-xl-5">
        <div class="col-lg-12 table-responsive mb-5">
            <table class="table table-light table-borderless table-hover text-center mb-0">
                <thead class="align-middle text-dark">
                    <tr>
                        <th class="align-middle">ID</th>
                        <th class="align-middle">Customer Name</th>
                        <th class="align-middle">Customer Email</th>
                        <th class="align-middle">Customer Phone</th>
                        <th class="align-middle">Customer Address</th>
                        <th class="align-middle">Note</th>
                        <th class="align-middle">Payment</th>
                        <th class="align-middle">Ship</th>
                        <th class="align-middle">Total</th>
                        <th class="align-middle">Status</th>
                        <th class="align-middle">Action</th>
                    </tr>
                </thead>
                <tbody class="align-middle">
                @foreach($orders as $item)
                    <tr id="row-{{$item->id}}">
                        <td class="align-middle">
                            {{$item->id}}
                        </td>
                        <td class="align-middle">
                            {{$item->customer_name}}
                        </td>
                        <td class="align-middle">
                            {{$item->customer_email}}
                        </td>
                        <td class="align-middle">
                            {{$item->customer_phone}}
                        </td>
                        <td class="align-middle">
                            {{$item->customer_address}}
                        </td>
                        <td class="align-middle">
                            {{$item->note}}
                        </td>
                        <td class="align-middle">
                            {{$item->payment}}
                        </td>
                        <td class="align-middle">
                            ${{$item->ship}}
                        </td>
                        <td class="align-middle">
                            ${{number_format($item->total)}}
                        </td>
                        <td class="align-middle">
                            <span class="badge bg-info text-dark">{{$item->status}}</span>
                        </td>
                        <td class="align-middle">
                            @if($item->status == 'pending')
                                <form action="{{route('client.orders.cancel', $item->id)}}" method="post" id="formCancel{{$item->id}}">
                                    @csrf
                                    <button class="btn btn-sm btn-danger btn-cancel" data-id="{{$item->id}}">Cancel</button>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-12 mb-5 d-flex justify-content-center">
            {{$orders->withQueryString()->links('pagination::bootstrap-4')}}
        </div>
    </div>
</div>
<!-- Cart End -->
@endsection
@section('script')
    <script src="{{asset('basic/basic.js')}}"></script>
    <script src="{{asset('client/app/order/order.js')}}"></script>
@endsection
