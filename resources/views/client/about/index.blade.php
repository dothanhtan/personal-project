@extends('client.layouts.app')
@section('title', 'About')
@section('content')
<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="{{route('client.home')}}">Home</a>
                <span class="breadcrumb-item active">About Us</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- About Start -->
<div class="bg-success py-5">
    <div class="container-fluid">
        <div class="row align-items-center px-xl-5 py-5">
            <div class="col-md-8 text-white">
                <h1 class="h1">
                    About Us
                </h1>
                <p class="text-justify">
                    Lorem ipsum dolor sit amet, consecrate disciplining elite, sed do usermod temper incident
                    ut labor et color magna aliquot. Ut enum ad minim venial, quits nostrum excitation McCull-och
                    labors nisei ut aliquot ex ea common consequent. Dis auto inure dolor in reprehend
                    in voluptuary valid ease cilium color eu fugit null paginator. Except saint toccata
                    cupidity non president, sunt in gulp qui official underused moll-it anim id est labor.

                    Lorem ipsum dolor sit amet, consecrate disciplining elite, sed do usermod temper incident
                    ut labor et color magna aliquot. Ege est lorem ipsum dolor sit. Sempre actor unique vitae
                    tempus qualm interpellates nec. Nil nun mi ipsum faucets. Morbid critique select et nets
                    et dalesman. At tells at urn condiment mattes interpellates. In curses turps mass incident
                    dui ut ornate lectures sit. Sit amet aliquot id diam nascence sultriness mi wget lauris.
                    Sit amet volute consequent Lauris. Veldt squelchier in dictum non consecrate a. Sultriness
                    lacks sed turps incident. Ac fugitive sed lectures vestibule mattes perambulator valid sed.
                    Rivera tells in hac habilitates plate dictum vestibule rhonchus. Id leo in vitae turps mass.
                    Ac placer vestibule lectures Lauris ult-rices eros in curses turps. Mattes perambulator valid
                    sed perambulator morbid incident ornate. Gravid quits bandit turps curses.
                </p>
            </div>
            <div class="col-md-4">
                <img src="{{asset('client/img/about-hero.svg')}}" alt="About Hero">
            </div>
        </div>
    </div>
</div>
<!-- About End -->

<!-- Services Start -->
<div class="bg-primary py-5">
    <div class="container-fluid">
        <div class="row text-center pt-5 pb-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">
                    Our Services
                </h1>
                <p class="text-justify">
                    Condiment mattes interpellates id nib. Deque vitae tempus qualm interpellates nec nam aliquot sem.
                    Vitae pron sagittal nil rhonchus mattes rhonchus urn unique riviera.
                    Aeneas pharaoh magna ac placer vestibule lectures lauris. Sed enum ut sem riviera aliquot wget.
                </p>
            </div>
        </div>
        <div class="row px-xl-5 pb-3">
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                    <h1 class="fa fa-check text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">Quality Product</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                    <h1 class="fa fa-shipping-fast text-primary m-0 mr-2"></h1>
                    <h5 class="font-weight-semi-bold m-0">Free Shipping</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                    <h1 class="fas fa-exchange-alt text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">14-Day Return</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                    <h1 class="fa fa-phone-volume text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">24/7 Support</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Services End -->

<!-- Brands Start -->
<div class="container-fluid py-5">
    <div class="row text-center py-3">
        <div class="col-lg-6 m-auto">
            <h1 class="h1">
                Our Brands
            </h1>
            <p class="text-justify">
                Lorem ipsum dolor sit amet, consecrate disciplining elite, sed do usermod temper incident
                ut labor et color magna aliquot. Tort at actor urn nun id curses metes aliquot. Venetians
                tells in metes amputate eu squelchier fells. Fauci bus ornate suspense sed nisei lacks
                sed riviera tells. Harare magna ac placer vestibule lectures lauris ult-rices eros.
            </p>
        </div>
    </div>
    <div class="row px-xl-5">
        <div class="col">
            <div class="owl-carousel vendor-carousel">
                <div class="bg-light p-4">
                    <img class="img-fluid w-100" src="{{asset('client/img/vendor-1.jpg')}}" alt="Brand Logo">
                </div>
                <div class="bg-light p-4">
                    <img class="img-fluid w-100" src="{{asset('client/img/vendor-2.jpg')}}" alt="Brand Logo">
                </div>
                <div class="bg-light p-4">
                    <img class="img-fluid w-100" src="{{asset('client/img/vendor-3.jpg')}}" alt="Brand Logo">
                </div>
                <div class="bg-light p-4">
                    <img class="img-fluid w-100" src="{{asset('client/img/vendor-4.jpg')}}" alt="Brand Logo">
                </div>
                <div class="bg-light p-4">
                    <img class="img-fluid w-100" src="{{asset('client/img/vendor-5.jpg')}}" alt="Brand Logo">
                </div>
                <div class="bg-light p-4">
                    <img class="img-fluid w-100" src="{{asset('client/img/vendor-6.jpg')}}" alt="Brand Logo">
                </div>
                <div class="bg-light p-4">
                    <img class="img-fluid w-100" src="{{asset('client/img/vendor-7.jpg')}}" alt="Brand Logo">
                </div>
                <div class="bg-light p-4">
                    <img class="img-fluid w-100" src="{{asset('client/img/vendor-8.jpg')}}" alt="Brand Logo">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Brands End-->
@endsection
