$(function () {
    $(document).on('click', '.btn-cancel', function (e) {
        e.preventDefault()
        let id = $(this).data('id')
        confirmDelete()
            .then(function () {
                $(`#formCancel${id}`).submit()
            })
            .catch()
    })
})
