$(() => {
    getTotalValue()

    function getTotalValue() {
        let subtotal = $('.subtotal').data('price')
        let coupon = $('.coupon')?.data('price') ?? 0
        let shipping = $('.shipping')?.data('price') ?? 0
        let total = subtotal - coupon + shipping
        let result = new Intl.NumberFormat().format(total)
        $('#total').val(`${total}`)
        $('.total').text(`$${result}`)
    }

    $(document).on('click', '.btn-update-quantity', function (e) {
        e.preventDefault()
        let url = $(this).data('action')
        let id = $(this).data('id')
        let data = { product_quantity: $(`#productQuantityInput-${id}`).val() }

        $.post(url, data, (res) => {
            let cartProductID = res.product_cart_id;
            let cart = res.cart;
            $('#productCountCart').text(cart.product_count);

            if (res.remove_product) {
                $(`#row-${cartProductID}`).remove()
            } else {
                $(`#cartProductPrice-${cartProductID}`).html(`$${res.cart_product_price}`);
            }
            getTotalValue();
            let subtotal = new Intl.NumberFormat().format(cart.total_price)
            let coupon = $('.coupon')?.data('price') ?? 0
            let total = new Intl.NumberFormat().format(cart.total_price - coupon)
            $('.subtotal').text(`$${subtotal}`)
            $('.total').text(`$${total}`)
            Swal.fire({
                position: "top-end",
                icon: "success",
                title: "success",
                showConfirmButton: false,
                timer: 1000,
            });
        });
    });

    $(document).on('click', '.btn-remove-product', function(e) {
        e.preventDefault()
        let url = $(this).data('action')
        confirmDelete()
            .then(function() {
                $.post(url, res => {
                    let cartProductID = res.product_cart_id;
                    let cart = res.cart;
                    let subtotal = new Intl.NumberFormat().format(cart.total_price)
                    let coupon = $('.coupon')?.data('price') ?? 0
                    let total = subtotal - coupon
                    let result = new Intl.NumberFormat().format(total)
                    $(".subtotal").text(`$${subtotal}`);
                    $('.total').text(`$${result}`);
                    $("#productCountCart").text(cart.product_count);
                    $(`#row-${cartProductID}`).remove();
                })
            })
            .catch(function (){});
    });
});
