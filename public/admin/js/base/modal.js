$(() => {
    renderSizes(sizes);
    appendSizesToForm();

    function renderSizes(sizes) {
        for (let size of sizes) {
            renderSize(size);
        }
    }

    function getSizeIndex(sizes, id) {
        let index = _.findIndex(sizes, function (key) {
            return key.id === id;
        });
        return index;
    }

    function removeSize(sizes, id) {
        let index = getSizeIndex(sizes, id);
        if (index >= 0) {
            $(`#product-size${sizes[index].id}`).remove();
            sizes.splice(index, 1);
        }
    }

    function addSize() {
        let size = {
            id: Date.now(),
            size: '30',
            quantity: 1,
        };
        sizes = [...sizes, size];
        renderSize(size);
    }

    function appendSizesToForm() {
        $("#inputSize").val(JSON.stringify(sizes));
    }

    $(document).on("click", ".btn-remove-size", function () {
        let id = $(this).data("id");
        removeSize(sizes, id);
    });

    $(document).on("click", ".btn-add-size", function () {
        addSize();
        appendSizesToForm();
    });

    $(document).on('keyup', '.input-size', function () {
        let id = $(this).data("id");
        let size = $(this).val();
        let index = getSizeIndex(sizes, id);

        if (index >= 0) {
            sizes[index].size = size;
        }

        appendSizesToForm();
    });

    $(document).on('keyup', '.input-quantity', function () {
        let id = $(this).data("id");
        let quantity = $(this).val();
        let index = getSizeIndex(sizes, id);

        if (index >= 0) {
            sizes[index].quantity = quantity;
        }

        appendSizesToForm();
    });

    function renderSize(size) {
        let html = `<div class="product-item-size" id="product-size${size.id}">
                        <div class="row align-items-center">
                            <div class="input-group input-group-static col-5" style="width: 40%">
                                <label>Size</label>
                                <input type="text" name="size" value="${size.size}" class="form-control input-size" data-id="${size.id}">
                            </div>

                            <div class="input-group input-group-static col-5" style="width: 40%">
                                <label>Quantity</label>
                                <input type="number" name="quantity" value="${size.quantity}" class="form-control input-quantity" data-id="${size.id}">
                            </div>

                            <div class="col-2" style="width: 20%">
                                <button type="button" class="btn btn-sm btn-danger btn-remove-size" data-id="${size.id}"><i class="fas fa-close"></i></button>
                            </div>
                        </div>`;
        $("#AddSize").append(html);
    }
});
