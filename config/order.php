<?php

return [
    'status' => [
        'pending' => 'pending',
        'accept' => 'accept',
        'delivery' => 'delivery',
        'success' => 'success',
        'cancel' => 'cancel'
    ]
];
