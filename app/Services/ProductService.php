<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;
use Illuminate\Http\Request;

class ProductService
{
    use HandleImage;
    /**
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * ProductService constructor
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function paginate($number)
    {
        return $this->productRepository->pagination($number);
    }

    public function relation($id)
    {
        return $this->productRepository->relation()->find($id);
    }

    public function getCategory($request, $catID)
    {
        $dataRequest = $request->all();
        return $this->productRepository->getCategory($dataRequest, $catID);
    }

    public function search(Request $request)
    {
        $productPrice = $request->product_price;
        $dataSearch['product_name'] = $request->product_name ?? null;
        $dataSearch['category_name'] = $request->category_name ?? null;
        $dataSearch['product_price'] = ($productPrice && ($productPrice != 0)) ? $productPrice : null;
        return $this->productRepository->search($dataSearch);
    }

    public function searchKeyWord(Request $request)
    {
        $dataSearch['keyword'] = $request->keyword ?? null;
        return $this->productRepository->searchKeyWord($dataSearch);
    }

    public function find($id)
    {
        return $this->productRepository->find($id);
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function create(Request $request)
    {
        $dataCreated = $request->except('sizes');
        $sizes = $request->sizes ? json_decode($request->sizes) : [];
        $product = $this->productRepository->create($dataCreated);
        $dataCreated['image'] = $this->saveImage($request);
        $product->images()->create(['url' => $dataCreated['image']]);

        if (!empty($request->category_ids)) {
            $product->assignCategory($dataCreated['category_ids']);
        }

        $listSizes = [];
        foreach ($sizes as $size) {
            $listSizes[] = ['size' => $size->size, 'quantity' => $size->quantity, 'product_id' => $product->id];
        }
        $product->details()->insert($listSizes);

        return $product;
    }

    public function update(Request $request, $id)
    {
        $dataUpdated = $request->except('sizes');
        $sizes = $request->sizes ? json_decode($request->sizes) : [];
        $product = $this->productRepository->findOrFail($id);
        $dataUpdated['category_ids'] = $request->category_ids ?? [];
        $currentImage =  $product->images->count() > 0 ? $product->images->first()->url : '';
        $dataUpdated['image'] = $this->updateImage($request, $currentImage);
        $product = $this->productRepository->update($dataUpdated, $id);
        $product->images()->delete();
        $product->images()->create(['url' => $dataUpdated['image']]);
        $product->assignCategory($dataUpdated['category_ids']);

        $listSizes = [];
        foreach ($sizes as $size) {
            $listSizes = ['size' => $size->size, 'quantity' => $size->quantity, 'product_id' => $product->id];
        }

        if ($product->details()->count() > 0 and $product->details()->count() < 3) {
            $product->details()->insert($listSizes);
        } else {
            $product->details()->delete();
        }

        return $product;
    }

    public function delete($id)
    {
        $product = $this->productRepository->find($id);
        $product->delete();
        $product->details()->delete();
        $imageName =  $product->images->count() > 0 ? $product->images->first()->url : '';
        $this->deleteImage($imageName);
        return $product;
    }
}
