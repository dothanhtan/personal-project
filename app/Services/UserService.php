<?php

namespace App\Services;

use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Traits\HandleImage;
use Illuminate\Support\Facades\Hash;

class UserService
{
    use HandleImage;
    /**
     * @var UserRepository $userRepository
     */
    protected UserRepository $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function all()
    {
        return $this->userRepository->getAll();
    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id)->load('roles');
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? null;
        $dataSearch['email'] = $request->email ?? null;
        $dataSearch['phone'] = $request->phone ?? null;
        return $this->userRepository->search($dataSearch);
    }

    public function create(StoreUserRequest $request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($dataCreate['password']);
        $dataCreate['image'] = $this->saveImage($request);
        $user = $this->userRepository->create($dataCreate);
        $user->images()->create(['url' => $dataCreate['image']]);

        if (!empty($request->role_ids)) {
            $user->roles()->attach($dataCreate['role_ids']);
        }

        return $user;
    }

    public function update($id, UpdateUserRequest $request)
    {
        $dataUpdate = $request->all();
        $user =  $this->userRepository->findOrFail($id)->load('roles');

        if (isset($dataUpdate['password'])) {
            $dataUpdate['password'] = Hash::make($dataUpdate['password']);
        } else {
            unset($dataUpdate['password']);
        }

        $currentImage =  $user->images->count() > 0 ? $user->images->first()->url : '';
        $dataUpdate['image'] = $this->updateImage($request, $currentImage);
        $user->update($dataUpdate);
        $user->images()->delete();
        $user->images()->create(['url' => $dataUpdate['image']]);
        $user->roles()->sync($dataUpdate['role_ids'] ?? []);
        return $user;
    }

    public function delete($id)
    {
        $user = $this->userRepository->findOrFail($id)->load('roles');
        $user->delete();
        $user->images()->delete();
        $imageName = $user->images->count() > 0 ? $user->images->first()->url : '';
        $this->deleteImage($imageName);
        return $user;
    }
}
