<?php

namespace App\Services;

use App\Repositories\CouponRepository;
use Illuminate\Http\Request;

class CouponService
{
    /**
     * @var $couponRepository
     */
    protected $couponRepository;

    /**
     * CouponService constructor.
     * @param CouponRepository $couponRepository
     */
    public function __construct(CouponRepository $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    public function all()
    {
        return $this->couponRepository->getAll();
    }

    public function findOrFail($id)
    {
        return $this->couponRepository->findOrFail($id);
    }

    public function search($request)
    {
        $dataSearch['coupon_name'] = $request->coupon_name ?? null;
        return $this->couponRepository->search($dataSearch);
    }

    public function create(Request $request)
    {
        $dataCreate = $request->all();
        return $this->couponRepository->create($dataCreate);
    }

    public function update(Request $request, $id)
    {
        $dataUpdate = $request->all();
        return $this->couponRepository->update($dataUpdate, $id);
    }

    public function delete(int $id)
    {
        return $this->couponRepository->delete($id);
    }
}
