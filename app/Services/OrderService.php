<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

class OrderService
{
    /**
     * @var OrderRepository $orderRepository
     */
    protected OrderRepository $orderRepository;

    /**
     * OrderService constructor.
     *
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function all()
    {
        return $this->orderRepository->getAll();
    }

    public function getCustomer()
    {
        return $this->orderRepository->getCustomer(auth()->id());
    }

    public function findOrFail($id)
    {
        return $this->orderRepository->findOrFail($id);
    }

    public function search(Request $request)
    {
        $dataSearch['customer_name'] = $request->customer_name ?? null;
        $dataSearch['customer_email'] = $request->customer_email ?? null;
        return $this->orderRepository->search($dataSearch);
    }

    public function update(Request $request, $id)
    {
        $dataUpdate = $request->all();
        return $this->orderRepository->update($dataUpdate, $id);
    }
}
