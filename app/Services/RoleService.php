<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleService
{
    /**
     * @var RoleRepository $roleRepository
     */
    protected RoleRepository $roleRepository;

    /**
     * RoleService constructor.
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->roleRepository->getAll();
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function search(Request $request)
    {
        $dataSearch['role_name'] = $request->role_name ?? null;
        return $this->roleRepository->search($dataSearch);
    }

    public function create(Request $request)
    {
        $dataCreate = $request->all();
        $dataCreate['guard_name'] = 'web';
        $role = $this->roleRepository->create($dataCreate);

        if (!empty($request->permission_ids)) {
            $role->permissions()->attach($dataCreate['permission_ids']);
        }

        return $role;
    }

    public function update(Request $request, $id)
    {
        $dataUpdate = $request->all();
        $dataUpdate['permission_ids'] = $request->permission_ids ?? [];
        $role = $this->roleRepository->update($dataUpdate, $id);
        $role->permissions()->sync($dataUpdate['permission_ids']);
        return $role;
    }

    public function delete(int $id)
    {
        return $this->roleRepository->delete($id);
    }
}
