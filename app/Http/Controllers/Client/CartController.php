<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\CreateOrderRequest;
use App\Http\Resources\Cart\CartResource;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    /**
     * @var Cart $cart
     * @var Product $product
     * @var CartProduct $cartProduct
     * @var Coupon $coupon
     * @var Order $order
     */
    protected Cart $cart;
    protected Product $product;
    protected CartProduct $cartProduct;
    protected Coupon $coupon;
    protected Order $order;

    /**
     * @param Cart $cart
     * @param Product $product
     * @param CartProduct $cartProduct
     * @param Coupon $coupon
     * @param Order $order
     */
    public function __construct(Cart $cart, Product $product, CartProduct $cartProduct, Coupon $coupon, Order $order)
    {
        $this->cart = $cart;
        $this->product = $product;
        $this->cartProduct = $cartProduct;
        $this->coupon = $coupon;
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = $this->cart->getCreatedBy(auth()->id())->load('products');
        return view('client.carts.index', compact('cart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->product_size) {
            $product = $this->product->findOrFail($request->product_id);
            $cart = $this->cart->getCreatedBy(auth()->id());
            $cartProduct = $this->cartProduct->getBy($cart->id, $product->id, $request->product_size);
            if ($cartProduct) {
                $quantity = $cartProduct->product_quantity;
                $productQuantity = $quantity + $request->product_quantity;
                $cartProduct->update(['product_quantity' => $productQuantity]);
            } else {
                $dataCreated['cart_id'] = $cart->id;
                $dataCreated['product_size'] = $request->product_size;
                $dataCreated['product_quantity'] = $request->product_quantity ?? 1;
                $dataCreated['product_price'] = $product->price;
                $dataCreated['product_id'] = $request->product_id;
                $this->cartProduct->create($dataCreated);
            }
            return back()->with('message', 'Add to cart success!');
        } else {
            return back()->with('message', 'Please choose size!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cartProduct =  $this->cartProduct->find($id);
        $dataUpdate = $request->all();
        if ($dataUpdate['product_quantity'] < 1) {
            $cartProduct->delete();
        } else {
            $cartProduct->update($dataUpdate);
        }

        $cart = $cartProduct->cart;

        return response()->json([
            'product_cart_id' => $id,
            'cart' => new CartResource($cart),
            'remove_product' => $dataUpdate['product_quantity'] < 1,
            'cart_product_price' => $cartProduct->total_price
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cartProduct = $this->cartProduct->findOrFail($id);
        $cartProduct->delete();
        $cart = $cartProduct->cart;
        return response()->json([
            'product_cart_id' => $id,
            'cart' => new CartResource($cart)
        ], Response::HTTP_OK);
    }

    public function applyCoupon(Request $request)
    {
        $name = $request->input('coupon_code');
        $coupon = $this->coupon->firstWithExperyDate($name, auth()->id());

        if ($coupon) {
            $message = 'Apply coupon success!';
            Session::put('coupon_id', $coupon->id);
            Session::put('coupon_code', $coupon->name);
            Session::put('coupon_value', $coupon->value);
        } else {
            Session::forget(['coupon_id', 'coupon_code', 'coupon_value']);
            $message = 'Coupon code does not exist or has expired!';
        }

        return to_route('client.carts.index')->with(['message' => $message]);
    }

    public function checkout()
    {
        $cart = $this->cart->getCreatedBy(auth()->id())->load('products');
        return view('client.carts.checkout', compact('cart'));
    }

    public function processCheckout(CreateOrderRequest $request)
    {
        $dataCreated = $request->all();
        $dataCreated['user_id'] = auth()->id();
        $dataCreated['status'] = 'pending';
        $this->order->create($dataCreated);
        $couponID = Session::get('coupon_id');
        if ($couponID) {
            $coupon = $this->coupon->find(Session::get('coupon_id'));
            if ($coupon) {
                $coupon->users()->attach(auth()->id(), ['value' => $coupon->value]);
            }
        }
        $cart = $this->cart->getCreatedBy(auth()->id());
        $cart->products()->delete();
        Session::forget(['coupon_id', 'coupon_code', 'coupon_value']);
        return to_route('client.carts.index')->with('message', 'Your order is placed successfully!');
    }
}
