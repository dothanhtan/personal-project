<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\OrderService;

class OrderController extends Controller
{
    /**
     * @var OrderService $orderService
     */
    protected OrderService $orderService;

    /**
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->orderService->getCustomer();
        return view('client.orders.index', compact('orders'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $order = $this->orderService->findOrFail($id);
        $order->update(['status' => 'cancel']);
        return to_route('client.orders.index')->with('message', 'This order is canceled successfully!');
    }
}
