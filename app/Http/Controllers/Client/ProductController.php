<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var ProductService $productService
     */
    protected ProductService $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = $this->productService->searchKeyWord($request);
        return view('client.products.index', compact('products'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $product = $this->productService->relation($id);
        $products = $this->productService->all();
        return view('client.products.detail', compact('product', 'products'));
    }

    /**
     * Show the product based on category the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $catID
     * @return \Illuminate\Http\Response
     */
    public function category(Request $request, int $catID)
    {
        $productCategories = $this->productService->getCategory($request, $catID);
        return view('client.products.category', compact('productCategories'));
    }
}
