<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\ProductService;

class HomeController extends Controller
{
    /**
     * @var ProductService $productService
     * @var CategoryService $categoryService
     */
    protected ProductService $productService;
    protected CategoryService $categoryService;

    /**
     * @param ProductService $productService
     * @param CategoryService $categoryService
     */
    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productService->paginate(12);
        $categories = $this->categoryService->getParentCategory();
        return view('client.home.index', compact('products', 'categories'));
    }
}
