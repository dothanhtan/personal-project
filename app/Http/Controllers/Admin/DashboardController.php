<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use App\Models\Role;
use App\Models\User;

class DashboardController extends Controller
{
    /**
     * @var Role $role
     * @var User $user
     * @var Category $category
     * @var Product $product
     * @var Coupon $coupon
     * @var Order $order
     */
    protected Role $role;
    protected User $user;
    protected Category $category;
    protected Product $product;
    protected Coupon $coupon;
    protected Order $order;

    /**
     * @param Role $role
     * @param User $user
     * @param Category $category
     * @param Product $product
     * @param Coupon $coupon
     * @param Order $order
     */
    public function __construct(
        Role $role,
        User $user,
        Category $category,
        Product $product,
        Coupon $coupon,
        Order $order
    ) {
        $this->role = $role;
        $this->user = $user;
        $this->category = $category;
        $this->product = $product;
        $this->coupon = $coupon;
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleCount = $this->role->count();
        $userCount = $this->user->count();
        $categoryCount = $this->category->count();
        $productCount = $this->product->count();
        $couponCount = $this->coupon->count();
        $orderCount = $this->order->count();
        return view(
            'admin.dashboard.index',
            compact(
                'roleCount',
                'userCount',
                'categoryCount',
                'productCount',
                'couponCount',
                'orderCount'
            )
        );
    }
}
