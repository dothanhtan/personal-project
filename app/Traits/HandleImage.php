<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    /**
     * @var string $path
     */
    protected string $path = 'upload/';

    public function verify($request)
    {
        return $request->has('image');
    }

    public function saveImage($request)
    {
        if ($this->verify($request)) {
            $image = $request->file('image');
            $name = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(350, 350)->save($this->path . $name);
            return $name;
        }
    }

    public function updateImage($request, $currentImage)
    {
        if ($this->verify($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }
        return $currentImage;
    }

    public function deleteImage($imageName)
    {
        if ($imageName && file_exists($this->path .$imageName)) {
            unlink($this->path .$imageName);
        }
    }
}
