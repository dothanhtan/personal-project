<?php

namespace App\Models;

use App\Traits\HandleImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, HandleImage;

    protected $fillable = ['name', 'description', 'sale', 'price'];

    public function details()
    {
        return $this->hasMany(ProductDetail::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product');
    }

    public function assignCategory($categoryIds)
    {
        return $this->categories()->sync($categoryIds);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', "%$name%") : null;
    }

    public function scopeWithPrice($query, $price)
    {
        return $price ? $query->where('price', '<=', $price) : null;
    }

    public function scopeWithCategoryName($query, $categoryName)
    {
        return $categoryName ? $query->whereHas('categories', fn($query)
                => $query->where('name', 'LIKE', "%$categoryName%")) : null;
    }

    public function scopeWithKeyword($query, $keyword)
    {
        return $keyword ? $query->where(function ($query) use ($keyword) {
            $query->where("name", "LIKE", "%$keyword%")->orWhere("price", "<=", $keyword);
        }) : null;
    }

    public function getBy($dataSearch, $catID)
    {
        return $this->whereHas('categories', fn($q) => $q->where('category_id', $catID))->paginate(6);
    }

    public function getImagePathAttribute()
    {
        return asset($this->images->count() > 0 ? 'upload/'.$this->images->first()->url : 'upload/default.jpg');
    }

    public function getSalePriceAttribute()
    {
        $priceSale = $this->attributes['sale'] * 0.01 * $this->attributes['price'];
        return $this->attributes['sale'] ? $this->attributes['price'] - $priceSale : 0;
    }
}
