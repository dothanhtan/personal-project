<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = ['user_id'];

    public function products()
    {
        return $this->hasMany(CartProduct::class, 'cart_id');
    }

    public function getBy($userId)
    {
        return Cart::whereUserId($userId)->first();
    }

    public function getCreatedBy($userId)
    {
        $cart = $this->getBy($userId);

        if (!$cart) {
            $cart = Cart::create(['user_id' => $userId]);
        }
        return $cart;
    }

    public function getProductCountAttribute()
    {
        return auth()->check() ? $this->products->count() : 0;
    }

    public function getTotalPriceAttribute()
    {
        return auth()->check() ? $this->products->reduce(function ($carry, $item) {
            $item->load('product');
            $productPrice = $item->product->sale ? $item->product->sale_price : $item->product->price;
            $price = $item->product_quantity * $productPrice;
            return $carry + $price;
        }, 0) : 0;
    }
}
