<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'status',
        'total',
        'ship',
        'payment',
        'customer_name',
        'customer_email',
        'customer_phone',
        'customer_address',
        'note',
    ];

    public function scopeWhereUserId($query, $user_id)
    {
        return $query->where("user_id", $user_id);
    }

    public function getWithPaginateBy($userID)
    {
        return $this->whereUserId($userID)->paginate(10);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('customer_name', 'LIKE', '%' . $name . '%') : null;
    }

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->where('customer_email', 'LIKE', '%' . $email . '%') : null;
    }
}
