<?php

namespace App\Repositories;

use App\Models\Coupon;

class CouponRepository extends BaseRepository
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return Coupon::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['coupon_name'])->paginate(5);
    }
}
