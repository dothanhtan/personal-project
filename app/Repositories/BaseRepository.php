<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * Get model
     * @return string
     */
    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make($this->getModel());
    }

    /**
     * BaseRepository constructor.
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * Retrieve all data of repository
     * @return Collection|Model[] `
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Retrieve all data of repository, paginated
     * @param null $limit
     * @param array $columns
     * @return
     */
    public function pagination($limit = null, array $columns = ['*'])
    {
        $limit = is_null($limit) ? config('repository.pagination.limit', 10) : $limit;
        return $this->model->paginate($limit, $columns);
    }

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * Get one
     *
     * @param $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Get by field
     * @param $column
     * @param $value
     * @return mixed
     */
    public function findByField($column, $value)
    {
        return $this->model->where($column, $value)->get();
    }

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attribute)
    {
        return $this->model->create($attribute);
    }

    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return bool|mixed
     */
    public function update(array $attributes, $id)
    {
        $result = $this->findOrFail($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }
        return false;
    }

    /**
     * Delete
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $result = $this->findOrFail($id);
        if ($result) {
            $result->delete();
            return true;
        }
        return false;
    }

    /**
     * Delete Array
     * @param array $ids
     * @return bool
     */
    public function destroyArray(array $ids)
    {
        return $this->model->destroy($ids);
    }
}
