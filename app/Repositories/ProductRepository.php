<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Product::class;
    }

    public function all()
    {
        return $this->model->with('details', 'categories')->get();
    }

    public function relation()
    {
        return $this->model->with('details')->get();
    }

    public function getCategory($request, $id)
    {
        return $this->model->getBy($request, $id);
    }

    public function find($id)
    {
        return $this->model->with('details', 'categories')->find($id);
    }

    public function findOrFail($id)
    {
        return $this->model->with('details', 'categories')->find($id);
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['product_name'])
            ->withPrice($dataSearch['product_price'])
            ->withcategoryName($dataSearch['category_name'])
            ->paginate(5);
    }

    public function searchKeyWord($dataSearch)
    {
        return $this->model->withKeyword($dataSearch['keyword'])->paginate(6);
    }
}
