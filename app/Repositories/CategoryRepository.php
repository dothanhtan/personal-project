<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return Category::class;
    }

    public function all()
    {
        return $this->model->with('childrens')->get();
    }

    public function find($id)
    {
        return $this->model->with('childrens')->find($id);
    }

    public function findOrFail($id)
    {
        return $this->model->with('childrens')->findOrFail($id);
    }

    public function getParentCategory()
    {
        return $this->model->getParents();
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['category_name'])->paginate(5);
    }
}
