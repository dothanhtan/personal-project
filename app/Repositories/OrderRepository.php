<?php

namespace App\Repositories;

use App\Models\Order;

class OrderRepository extends BaseRepository
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return Order::class;
    }

    public function getCustomer($userID)
    {
        return $this->model->whereUserId($userID)->paginate(10);
    }
}
