## 1. [Install and run laravel](https://laravel.com/docs/9.x#installation-via-composer)
```bash
    composer create-project laravel/laravel laravel-project

    cd example-app

    php artisan serve
```

## 2. [Use laravel/ui to install Auth](https://www.itsolutionstuff.com/post/laravel-9-bootstrap-auth-scaffolding-tutorialexample.html)
```bash
    composer require laravel/ui

    php artisan ui bootstrap --auth

    npm install && npm run dev

    php artisan migrate
```

## 3. [Install authorize with laravel](https://spatie.be/docs/laravel-permission/v5/installation-laravel)
1. You can install the package via composer:
```bash
    composer require spatie/laravel-permission
```

2. Optional: The service provider will automatically get registered. Or you may manually add the service provider in your config/app.php file:
```
    'providers' => [
        // ...
        Spatie\Permission\PermissionServiceProvider::class,
    ];
```

3. You should publish the migration and the config/permission.php config file with:
```bash
    php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"
```

4. Clear your config cache:
```bash
    php artisan optimize:clear
```

5. Run the migrations:
```bash
    php artisan migrate
```

## 4. [How to upload image file](https://image.intervention.io/v2/usage/overview)
1. Install Intervention\Image
```bash
    // include composer autoload
    require 'vendor/autoload.php';

    // import the Intervention Image Manager Class
    use Intervention\Image\ImageManagerStatic as Image;
```

2. Read image from file
```bash
    // configure with favored image driver (gd by default)
    Image::configure(['driver' => 'imagick']);

    // and you are ready to go ...
    $image = Image::make('public/foo.jpg')->resize(300, 200);
```

## 5. [Install PHPCS and test](https://gitlab.com/dothanhtan/setup-laravel)
1. Install PHPCS
```bash
    composer require --dev squizlabs/php_codesniffer
```

2. Test folder by using convention.sh
```bash
    # syntax: phpcs --standard=PSR2 <folder patch>
    # example:
    echo "Testing PSR-2"
    ./vendor/bin/phpcs --standard=PSR2 --sniffs=Generic.PHP.LowerCaseConstant tests
    ./vendor/bin/phpcs --standard=PSR2 app/Traits
    ./vendor/bin/phpcs --standard=PSR2 app/Traits
    ./vendor/bin/phpcs --standard=PSR2 app/Models
    ./vendor/bin/phpcs --standard=PSR2 app/Services
    ./vendor/bin/phpcs --standard=PSR2 app/Repositories
    ./vendor/bin/phpcs --standard=PSR2 app/Http/Controllers
```

## 6. [Install PHPCBF to auto fix error](https://gitlab.com/dothanhtan/setup-laravel)
1. Install PHPBF
```bash
    composer require --dev "squizlabs/php_codesniffer=*"
```

2. Auto fix error folder by using convention.sh
```bash
    # syntax: phpcbf --standard=PSR2 <folder patch>
    # example:
    echo "Auto fix error PSR-2"
    ./vendor/bin/phpcbf --standard=PSR2 --sniffs=Generic.PHP.LowerCaseConstant tests
    ./vendor/bin/phpcbf --standard=PSR2 app/Traits
    ./vendor/bin/phpcbf --standard=PSR2 app/Models
    ./vendor/bin/phpcbf --standard=PSR2 app/Services
    ./vendor/bin/phpcbf --standard=PSR2 app/Repositories
    ./vendor/bin/phpcbf --standard=PSR2 app/Http/Controllers
```

## 7. Choose template for project
1. Admin
    [Creative Tim](https://www.creative-tim.com/bootstrap-themes/free)

2. Client
    [Multi Shop](https://drive.google.com/drive/folders/1bLnr3CmrjSdfNrUEgxarjcBCLqky6R3R?usp=sharing)

## 8. Design database for project
1. Use link
    [app.diagrams.net](https://app.diagrams.net)
    
2. My database design
    [My database](https://drive.google.com/file/d/1h7BAeYdRg8qrWQzxHXlJ7FNdsSTvB7zl/view?usp=sharing)
